#!/usr/bin/perl
#
#  ChDpredictCon
#
#  Created by Eduardo Andrés León on 2016-11-05.
#  Copyright (c) 2017 IPBLN. All rights reserved.
#

$|=1;

use strict;
use Getopt::Long;
use Date::Calc qw/Delta_Days/;
use File::Basename;
use Excel::Writer::XLSX;

my $help;
my $database="../Data/Database_of_four_biomarkers_at_3_timePoints.txt";
my $run;
my $verbose=undef;
my $label=undef;

GetOptions(
	"help" => \$help,
	"run" => \$run,
	"db|d=s" => \$database,
	"verbose" => \$verbose,
	"label|l=s" => \$label
);

if($run){
	open(DB,$database) || die "$!: Cant read $database\n";
	print '=' x 100 ."\n";
	print STDERR date(). " Reading database: $database\n";
	my $db_data;
	while(<DB>){
		chomp;
		if($_ !~/^ID/){
			if($_ =~ /NA\t/){
				print "Wrong line $_\n" if($verbose);
			}
			else{
				my($paciente,$K0,$K1,$K2,$H0,$H1,$H2,$P0,$P1,$P2,$I0,$I1,$I2,$patologia)=split(/\t/);

				$db_data->{id}->{$paciente}++;
				$db_data->{patologia}->{$patologia}++;
				$db_data->{$paciente}->{"BioM1-T1"}=$K0;
				$db_data->{$paciente}->{"BioM1-T2"}=$K1;
				$db_data->{$paciente}->{"BioM1-T3"}=$K2;
				$db_data->{$paciente}->{"BioM2-T1"}=$H0;
				$db_data->{$paciente}->{"BioM2-T2"}=$H1;
				$db_data->{$paciente}->{"BioM2-T3"}=$H2;
				$db_data->{$paciente}->{"BioM3-T1"}=$P0;
				$db_data->{$paciente}->{"BioM3-T2"}=$P1;
				$db_data->{$paciente}->{"BioM3-T3"}=$P2;
				$db_data->{$paciente}->{"BioM4-T1"}=$I0;
				$db_data->{$paciente}->{"BioM4-T2"}=$I1;
				$db_data->{$paciente}->{"BioM4-T3"}=$I2;
				$db_data->{$paciente}->{"Grado"}=$patologia;
				
			}
		}
	}
	close DB;
	print STDERR date(). " Total number of patients: ".scalar(keys %{$db_data->{id}}) ."\n";
	print STDERR date(). " Total number of pathologies: ".scalar(keys %{$db_data->{patologia}}) ."\n";
	
	####################################
	#  Predict therapeutic efficacy    #
	####################################
		
	my $results1;
	my $detailed_results1;
	
	my $results2;
	my $detailed_results2;
	
	my $results1;
	my $detailed_results1;
	
	my $all_results;
	
	print '=' x 100 ."\n";
	print "\n" . date() . "\n";		
	
	#Filter 1
	foreach my $patient (sort keys %{$db_data->{id}}){
		my $condition_succesfull;
		
		#First condition: in all biomarkers, the expresion in T0 must be greater than the expression in T24
		if( ($db_data->{$patient}->{"BioM1-T1"} >= $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM2-T1"} >= $db_data->{$patient}->{"BioM2-T3"} ) and ( $db_data->{$patient}->{"BioM3-T1"} >= $db_data->{$patient}->{"BioM3-T3"} ) and ( $db_data->{$patient}->{"BioM4-T1"} >= $db_data->{$patient}->{"BioM4-T3"} )){
			$condition_succesfull++;
		
			#Second condition to be fulfilled by at least 2 markers
			#1 pair of markers BioM1-BioM3
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM1-T2"} > $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="1-2\tBioM1-BioM3";
						$results1->{$patient}++;
						$detailed_results1->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#2 pair of markers BioM1-BioM2
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T3"}) eq "T") and ( c30_percent($db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM1-T2"} > $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM2-T2"} > $db_data->{$patient}->{"BioM2-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="1-2\tBioM1-BioM2";
						$results1->{$patient}++;
						$detailed_results1->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#3 pair of markers BioM1-BioM4
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM1-T2"} > $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM4-T2"} > $db_data->{$patient}->{"BioM4-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="1-2\tBioM1-BioM4";
						$results1->{$patient}++;
						$detailed_results1->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#4 pair of markers BioM3-BioM2
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T") and ( c30_percent($db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"}) and ( $db_data->{$patient}->{"BioM2-T2"} > $db_data->{$patient}->{"BioM2-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="1-2\tBioM3-BioM2";
						$results1->{$patient}++;
						$detailed_results1->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#5 pair of markers BioM3-BioM4
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"}) and ( $db_data->{$patient}->{"BioM4-T2"} > $db_data->{$patient}->{"BioM4-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="1-2\tBioM3-BioM4";
						$results1->{$patient}++;
						$detailed_results1->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#6 pair of markers BioM2-BioM4
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (c30_percent($db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM2-T2"} > $db_data->{$patient}->{"BioM2-T3"}) and ( $db_data->{$patient}->{"BioM4-T2"} > $db_data->{$patient}->{"BioM4-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="1-2\tBioM2-BioM4";
						$results1->{$patient}++;
						$detailed_results1->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}

			#Third condition to be fulfilled by at least 2 markers
			#1 pair of markers BioM1-BioM3
			#filter a
			if( (c20_percent($db_data->{$patient}->{"BioM1-T3"},$db_data->{$patient}->{"BioM1-T2"}) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM3-T3"},$db_data->{$patient}->{"BioM3-T2"}) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM1-T2"},$db_data->{$patient}->{"BioM1-T1"},$patient) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM3-T2"},$db_data->{$patient}->{"BioM3-T1"},$patient) eq "T")){
					#Patients in this block meet all the conditions imposed
					my $msg="1-3\tBioM1-BioM3";
					$results1->{$patient}++;
					$detailed_results1->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#2 pair of markers BioM1-BioM2
			#filter a
			if( (c20_percent($db_data->{$patient}->{"BioM1-T3"},$db_data->{$patient}->{"BioM1-T2"}) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM2-T3"},$db_data->{$patient}->{"BioM2-T2"}) eq "T")){
				#filter b				
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM1-T2"},$db_data->{$patient}->{"BioM1-T1"},$patient) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM2-T2"},$db_data->{$patient}->{"BioM2-T1"},$patient) eq "T")){
					#Patients in this block meet all the conditions imposed
					my $msg="1-3\tBioM1-BioM2";
					$results1->{$patient}++;
					$detailed_results1->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#3 pair of markers BioM1-BioM4
			#filter a
			if( (c20_percent($db_data->{$patient}->{"BioM1-T3"},$db_data->{$patient}->{"BioM1-T2"}) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM4-T3"},$db_data->{$patient}->{"BioM4-T2"}) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM1-T2"},$db_data->{$patient}->{"BioM1-T1"},$patient) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM4-T2"},$db_data->{$patient}->{"BioM4-T1"},$patient) eq "T")){
					#Patients in this block meet all the conditions imposed
					my $msg="1-3\tBioM1-BioM4";
					$results1->{$patient}++;
					$detailed_results1->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#4 pair of markers BioM3-BioM2
			#filter a
			if( (c20_percent($db_data->{$patient}->{"BioM3-T3"},$db_data->{$patient}->{"BioM3-T2"}) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM2-T3"},$db_data->{$patient}->{"BioM2-T2"}) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM3-T2"},$db_data->{$patient}->{"BioM3-T1"},$patient) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM2-T2"},$db_data->{$patient}->{"BioM2-T1"},$patient) eq "T")){
					#Patients in this block meet all the conditions imposed
					my $msg="1-3\tBioM3-BioM2";
					$results1->{$patient}++;
					$detailed_results1->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#5 pair of markers BioM3-BioM4
			#filter a
			if( (c20_percent($db_data->{$patient}->{"BioM3-T3"},$db_data->{$patient}->{"BioM3-T2"}) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM4-T3"},$db_data->{$patient}->{"BioM4-T2"}) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM3-T2"},$db_data->{$patient}->{"BioM3-T1"},$patient) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM4-T2"},$db_data->{$patient}->{"BioM4-T1"},$patient) eq "T")){
					#Patients in this block meet all the conditions imposed
					my $msg="1-3\tBioM3-BioM4";
					$results1->{$patient}++;
					$detailed_results1->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#6 pair of markers BioM2-BioM4
			#filter a
			if( (c20_percent($db_data->{$patient}->{"BioM2-T3"},$db_data->{$patient}->{"BioM2-T2"}) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM4-T3"},$db_data->{$patient}->{"BioM4-T2"}) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM2-T2"},$db_data->{$patient}->{"BioM2-T1"},$patient) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM4-T2"},$db_data->{$patient}->{"BioM4-T1"},$patient) eq "T")){
					#Patients in this block meet all the conditions imposed
					my $msg="1-3\tBioM2-BioM4";
					$results1->{$patient}++;
					$detailed_results1->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
		}
	}
	
	#Filter 2
	foreach my $patient (sort keys %{$db_data->{id}}){
		my $condition_succesfull;

		#First condition: in all biomarkers, the expresion in T0 must be greater than the expression in T24
		if( ($db_data->{$patient}->{"BioM1-T1"} >= $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM2-T1"} >= $db_data->{$patient}->{"BioM2-T3"} ) and ( $db_data->{$patient}->{"BioM3-T1"} >= $db_data->{$patient}->{"BioM3-T3"} ) and ( $db_data->{$patient}->{"BioM4-T1"} >= $db_data->{$patient}->{"BioM4-T3"} )){
			$condition_succesfull++;

			#Second condition to be fulfilled by at least 3 markers
			#1 trio of markers BioM1-BioM3-BioM2
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T" ) and ( c30_percent($db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM1-T2"} > $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"} ) and ( $db_data->{$patient}->{"BioM2-T2"} > $db_data->{$patient}->{"BioM2-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="4-5\tBioM1-BioM3-BioM2";
						$results2->{$patient}++;
						$detailed_results2->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#2 trio of markers BioM1-BioM2-BioM4
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T" ) and ( c40_percent($db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM1-T2"} > $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"} ) and ( $db_data->{$patient}->{"BioM4-T2"} > $db_data->{$patient}->{"BioM4-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="4-5\tBioM1-BioM3-BioM4";
						$results2->{$patient}++;
						$detailed_results2->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#3 trio of markers BioM2-BioM3-BioM4
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T" ) and ( c30_percent($db_data->{$patient}->{"BioM2-T1"},$db_data->{$patient}->{"BioM2-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM4-T2"} > $db_data->{$patient}->{"BioM4-T3"}) and ( $db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"} ) and ( $db_data->{$patient}->{"BioM2-T2"} > $db_data->{$patient}->{"BioM2-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="4-5\tBioM1-BioM3-BioM2";
						$results2->{$patient}++;
						$detailed_results2->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			#4 trio of markers BioM1-BioM3-BioM4
			#filter a
			if( (range_percent(0,35,$db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( range_percent(0,35,$db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (c40_percent($db_data->{$patient}->{"BioM1-T1"},$db_data->{$patient}->{"BioM1-T3"}) eq "T") and ( c40_percent($db_data->{$patient}->{"BioM3-T1"},$db_data->{$patient}->{"BioM3-T3"}) eq "T" ) and ( c40_percent($db_data->{$patient}->{"BioM4-T1"},$db_data->{$patient}->{"BioM4-T3"}) eq "T" )){
					# filter c
					if( ($db_data->{$patient}->{"BioM1-T2"} > $db_data->{$patient}->{"BioM1-T3"}) and ( $db_data->{$patient}->{"BioM3-T2"} > $db_data->{$patient}->{"BioM3-T3"} ) and ( $db_data->{$patient}->{"BioM4-T2"} > $db_data->{$patient}->{"BioM4-T3"} )){
						#Patients in this block meet all the conditions imposed
						my $msg="4-5\tBioM1-BioM3-BioM4";
						$results2->{$patient}++;
						$detailed_results2->{$patient}->{$msg}++;
						$all_results->{$patient}++;
						if($verbose){
							print $patient ."\t$msg\n";
						}
					}
				}
			}
			
			#Third condition to be fullfilled by at least 3 markers
			#1 trio of markers BioM1-BioM3-BioM2
			if( (c20_percent($db_data->{$patient}->{"BioM1-T3"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM3-T3"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM2-T3"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM1-T2"},$db_data->{$patient}->{"BioM1-T1"}) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM3-T2"},$db_data->{$patient}->{"BioM3-T1"}) eq "T" ) and ( range_percent(60,100,$db_data->{$patient}->{"BioM2-T2"},$db_data->{$patient}->{"BioM2-T1"}) eq "T" )){
					#Patients in this block meet all the conditions imposed
					my $msg="4-6\tBioM1-BioM3-BioM2";
					$results2->{$patient}++;
					$detailed_results2->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#2 trio of markers BioM1-BioM2-BioM4
			if( (c20_percent($db_data->{$patient}->{"BioM1-T3"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM2-T3"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM4-T3"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM1-T2"},$db_data->{$patient}->{"BioM1-T1"}) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM2-T2"},$db_data->{$patient}->{"BioM2-T1"}) eq "T" ) and ( range_percent(60,100,$db_data->{$patient}->{"BioM4-T2"},$db_data->{$patient}->{"BioM4-T1"}) eq "T" )){
					#Patients in this block meet all the conditions imposed
					my $msg="4-6\tBioM1-BioM2-BioM4";
					$results2->{$patient}++;
					$detailed_results2->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#3 trio of markers BioM2-BioM3-BioM4
			if( (c20_percent($db_data->{$patient}->{"BioM2-T3"},$db_data->{$patient}->{"BioM2-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM3-T3"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM4-T3"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM2-T2"},$db_data->{$patient}->{"BioM2-T1"}) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM3-T2"},$db_data->{$patient}->{"BioM3-T1"}) eq "T" ) and ( range_percent(60,100,$db_data->{$patient}->{"BioM4-T2"},$db_data->{$patient}->{"BioM4-T1"}) eq "T" )){
					#Patients in this block meet all the conditions imposed
					my $msg="4-6\tBioM2-BioM3-BioM4";
					$results2->{$patient}++;
					$detailed_results2->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
			#4 trio of markers BioM1-BioM3-BioM4
			if( (c20_percent($db_data->{$patient}->{"BioM1-T3"},$db_data->{$patient}->{"BioM1-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM3-T3"},$db_data->{$patient}->{"BioM3-T2"},$patient) eq "T") and ( c20_percent($db_data->{$patient}->{"BioM4-T3"},$db_data->{$patient}->{"BioM4-T2"},$patient) eq "T")){
				#filter b
				if( (range_percent(60,100,$db_data->{$patient}->{"BioM1-T2"},$db_data->{$patient}->{"BioM1-T1"}) eq "T") and ( range_percent(60,100,$db_data->{$patient}->{"BioM3-T2"},$db_data->{$patient}->{"BioM3-T1"}) eq "T" ) and ( range_percent(60,100,$db_data->{$patient}->{"BioM4-T2"},$db_data->{$patient}->{"BioM4-T1"}) eq "T" )){
					#Patients in this block meet all the conditions imposed
					my $msg="4-6\tBioM1-BioM3-BioM4";
					$results2->{$patient}++;
					$detailed_results2->{$patient}->{$msg}++;
					$all_results->{$patient}++;
					if($verbose){
						print $patient ."\t$msg\n";
					}
				}
			}
		}
	}
	
	#printing in excel format
	my $workbook  = Excel::Writer::XLSX->new(outfile($label));
	my $bold = $workbook->add_format(bold => 1);		
	my $worksheet_t = $workbook->add_worksheet("Total records");
	
	my $count=0;
	$worksheet_t->write(0,0,"Patient",$bold);
	$worksheet_t->write(0,1,"Filter 1",$bold);
	$worksheet_t->write(0,2,"Filter 2",$bold);
	$worksheet_t->write(0,3,"Degree of pathology",$bold);
	
	foreach my $subject (sort keys %{$all_results}){
		$count++;
		my $A="-";
		if(exists $results1->{$subject}){
			$A="Yes";
		}
		my $B="-";
		if(exists $results2->{$subject}){
			$B="Yes";
		}
		$worksheet_t->write($count,0,$subject);
		$worksheet_t->write($count,1,$A);
		$worksheet_t->write($count,2,$B);
		$worksheet_t->write($count,3,$db_data->{$subject}->{"Grado"});
	}

	my $count=0;
	my $worksheet_dA = $workbook->add_worksheet("Filter 1 details");
	$worksheet_dA->write(0,0,"Patient",$bold);
	$worksheet_dA->write(0,1,"Filters",$bold);
	$worksheet_dA->write(0,2,"Conditional genes",$bold);

	my $count=0;
	foreach my $subject (sort keys %{$detailed_results1}){
		foreach my $gen (sort keys %{$detailed_results1->{$subject}}){
			$count++;
			my($filter,$markers)=split(/\t/,$gen);
			
			$worksheet_dA->write($count,0,$subject);
			$worksheet_dA->write($count,1,$filter);
			$worksheet_dA->write($count,2,$markers);
		}
	
	}

	my $worksheet_dB = $workbook->add_worksheet("Filter 2 details");
	$worksheet_dB->write(0,0,"Patient",$bold);
	$worksheet_dB->write(0,1,"Filters",$bold);
	$worksheet_dB->write(0,2,"Conditional genes",$bold);

	my $count=0;
	foreach my $subject (sort keys %{$detailed_results2}){
		foreach my $gen (sort keys %{$detailed_results2->{$subject}}){
			$count++;
			my($filter,$markers)=split(/\t/,$gen);
			$worksheet_dB->write($count,0,$subject);
			$worksheet_dB->write($count,1,$filter);
			$worksheet_dB->write($count,2,$markers);
			
		}
	
	}
	# my $worksheet_dC = $workbook->add_worksheet("Filter 3 details");
	# $worksheet_dC->write(0,0,"Patient",$bold);
	# $worksheet_dC->write(0,1,"Conditional genes",$bold);
	#
	# my $count=0;
	# foreach my $subject (sort keys %{$detailed_results1}){
	# 	foreach my $gen (sort keys %{$detailed_results1->{$subject}}){
	# 		$count++;
	# 		$worksheet_dC->write($count,0,$subject);
	# 		$worksheet_dC->write($count,1,$gen);
	# 	}
	#
	# }
	
	
	$workbook->close;

}
elsif($help){
	help();
}
else{
	help();
}
sub help{
 my $usage = qq{
   $0 

     Getting help:
         [--help]

     Needed parameters:
       [run] : Execute $0        

     Optional parameters:
       [db|d] : Path to a patient database
	   [label|l] : Label to include in output file. If label contains spaces, please use quotation marks -label "This is a label"          
       [verbose] : When execute it with -run print details in your screen         
       [help] : Print this         

     Examples:
        Predict therapeutic efficacy in default database:
		
            $0 -run
	    
        Predict therapeutic efficacy in a different database and include a label:
		
            $0 -run -label "New analysis" -db new_database.txt
			
 };

 print STDERR $usage;
 exit();
        
}

sub date{
	use Time::localtime;
	my $now = ctime();
	return("[$now]");
}
sub outfile($){
	my $label=shift;
	use Time::localtime;
	my $now = ctime();
	my(undef,$month,$day,$time,$year)=split(/\s+/,$now);

	if(defined($label)){
		return("Therapeutic_Efficacy_$label\_". $month ."_". $day ."_". $year."_".  int(rand(1000000)) .".xlsx");		
	}
	else{
		return("Therapeutic_Efficacy_". $month ."_". $day ."_". $year."_".  int(rand(1000000)) .".xlsx");
	}
}
sub c40_percent($$){
	my $maximo=shift;
	my $valor=shift;
	
	my $perc=((($maximo/$valor)*100) - 100 );
	if($perc>40){
		return("T");
	}
	else{
		return("F");
	}
}
sub c30_percent($$){
	my $maximo=shift;
	my $valor=shift;
	
	my $perc=((($maximo/$valor)*100) - 100 );
	if($perc>30){
		return("T");
	}
	else{
		return("F");
	}
}
sub c20_percent($$){
	my $maximo=shift;
	my $valor=shift;
	
	my $perc=((($maximo/$valor)*100) - 100 );
	if($perc>20){
		return("T");
	}
	else{
		return("F");
	}
}
sub range_percent($$$$;$){
	
	my $min_perc=shift;
	my $max_perc=shift;
	my $valor=shift;
	my $maximo=shift;
	my $paciente=shift;
	
	my $perc=((($maximo/$valor)*100) - 100 );
		
	if($perc<=$max_perc){
		return("T");
	}
	else{
		return("F");
	}
}

__END__

=encoding ISO8859-1
=head1 NAME

Chagas predictor control - A script to predict therapeutic efficacy in treated Chagas disease patients.

=head1 SYNOPSIS

ChDpredictCon is a software designed to evaluate treatment efficacy in treated chronic Chagas disease patients based on a drop in the reactivity of patient sera against four T. cruzi biomarkers after treatment.

=head1 DESCRIPTION

The program requires the reactivity data of patient sera against the four antigens (as OD values) at least at three time intervals (one before treatment and at least two after treatment). Two criteria are applied and require a drop in reactivity against the four biomarkers after treatment regarding the reactivity measured before treatment (T9<T0, T24 or T48<T0). In addition, a continuous and significant decrease in the reactivity against two (Condition A) or three (Condition B) out of four biomarkers is required. It implies that the reactivity have to drop at T24 or T48<T0 by 40% (KMP11, PFR2 and 3973) or by 30% (HSP70), and also drop at 9 months (T9)<T0  and T9<T24 or T48. Cases in which T9≥T0 up to 35% are allowed. 

=head1 Command Line Options

Command line operated programs traditionally take their arguments from the command line, for example filenames or other information that the program needs to know. Besides arguments, these programs often take command line options as well. Options are not necessary for the program to work, hence the name 'option', but are used to modify its default behaviour. For example, a program could do its job quietly, but with a suitable option it could provide verbose information about what it did.

Command line options come in several flavours. Historically, they are preceded by a single dash "-", and consist of a single letter. For ChDpredictCon there is one needed argument and four optional arguments. They are explained below:
 
=head2 "run" mandatory argument: 

In order to execute ChDpredictCon with our providad data, you only need to use the "-run" argument:
		    
   perl ChDpredictCon.pl -run
		   
This command will read the provided database and it will create an excel file containing all results.
		   
=head2 "db" optional argument: 

The db argument or d has been introduced to provided additional databases from other patients or other biomarkers, as an example:

   perl ChDpredictCon.pl -run -db ../Data/Database_of_new_patients.txt 

=head2 "label" optional argument: 

The label argument or l helps you to inclue a label name in the output excel file. This is invoked by: 
   
   perl ChDpredictCon.pl -run -label "New analysis"
    
=head2 "verbose" optional argument: 
		   
The verbose argument print detailed information over your screen.

=head1 HELP

Executing ChDpredictCon with no arguments, will print some help to guide you.
	   
=head1