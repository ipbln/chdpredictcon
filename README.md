# Chagas predictor control - ChDpredictCon#

**ChDpredictCon** is a software designed to evaluate treatment efficacy in treated chronic Chagas disease patients based on a drop in the reactivity of patient sera against four T. cruzi biomarkers after treatment.

The program requires the reactivity data of patient sera against the four antigens (as OD values) at least at three time intervals (one before treatment and at least two after treatment). Two criteria are applied and require a drop in reactivity against the four biomarkers after treatment regarding the reactivity measured before treatment (T9<T0, T24 or T48<T0). In addition, a continuous and significant decrease in the reactivity against two (Condition A) or three (Condition B) out of four biomarkers is required. It implies that the reactivity have to drop at T24 or T48<T0 by 40% (KMP11, PFR2 and 3973) or by 30% (HSP70), and also drop at 9 months (T9)<T0  and T9<T24 or T48. Cases in which T9≥T0 up to 35% are allowed. 


    ChDpredictCon has been created at The Institute of Parasitology and Biomedicine "López-Neyra" (IPBLN)
    Copyright (c) 2017 IPBLN. All rights reserved.

### 1. Pre-requisites ###

#### Perl ####
Perl (Practical Extraction and Reporting Language) can be installed in all kind of platforms:

* **Windows**. To install Perl in a windows machine, pelase follow this [steps](https://learn.perl.org/installing/windows.html).
* **Linux**. Perl is installed by default in all Unix machines.
* **Apple**. Perl is also installed by default in all Apple computers, but Xcode is required to install perl modules. Use this link to install [XCode](https://itunes.apple.com/es/app/xcode/id497799835?l=en&mt=12).

#### Perl modules####
ChDpredictCon stores all results in an excel file. So a [pluglin](http://search.cpan.org/~jmcnamara/Excel-Writer-XLSX/lib/Excel/Writer/XLSX.pm) to create this kind of files is needed.
 This plugin can be installed in any kind of machines having perl by invoking the following command:
     
	 cpan Excel::Writer::XLSX


### 2. Usage ###

     Getting help:
         [--help]

     Needed parameters:
       [run] : Execute ./ChDpredictCon.pl        

     Optional parameters:
       [db|d] : Path to a patient database
	   [label|l] : Label to include in output file. If label contains spaces, please use quotation marks -label "This is a label"          
       [verbose] : When execute it with -run print details in your screen         
       [help] : Print this         

     Examples:
        Predict therapeutic efficacy in default database:
		
            ./ChDpredictCon.pl -run
	    
        Predict therapeutic efficacy in a different database and include a label:
		
            ./ChDpredictCon.pl -run -label "New analysis" -db new_database.txt

### 3. Documentation ####
Documentation is available within the software. You can consult the documentation by executing the following command:
    
	perldoc ChDpredictCon.pl
	
### 4. Who do I talk to? ###
* mcthomas@ipb.csic.es, bioinformatica@ipb.csic.es